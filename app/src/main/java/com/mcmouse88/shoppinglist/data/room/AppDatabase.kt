package com.mcmouse88.shoppinglist.data.room

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Создадим класс базы данных, для этого пометим его аннотацией [Database], и передадим
 * в качестве параметров, наш класс [ProductDbModel], версию и экспорт схемы, и сделаем класс
 * абстрактным, и будем наследоваться от класса [RoomDatabase]
 */
@Database(entities = [ProductDbModel::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun productListDao(): ProductListDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        private val LOCK = Any()
        private const val DB_NAME = "product_database.db"

        fun getInstance(application: Application): AppDatabase {
            INSTANCE?.let {
                return it
            }
            synchronized(LOCK) {
                INSTANCE?.let {
                    return it
                }
            }
            val database = Room.databaseBuilder(
                application,
                AppDatabase::class.java,
                DB_NAME
            )
                // .allowMainThreadQueries() // Разрешить делать запросы в главном потоке, но так делать не стоит
                .build()
            INSTANCE = database
            return database
        }
    }
}