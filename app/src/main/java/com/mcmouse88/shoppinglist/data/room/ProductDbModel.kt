package com.mcmouse88.shoppinglist.data.room

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Создадим класс дублер класса [Product], чтобы не нарушать принципы чистой архитектуры,
 * так как domain слой не должен ни от чего зависеть, а в этом случае он будет зависеть
 * от бызы данных.
 * Чтобы связать data класс с базой данных, его нужно пометить аннотацией [Entity],
 * и в скобочка поместить название таблицы. Также у нас в базе данных должен быть
 * первичный ключ, в качестве которого мы будем использовать свойство [id], и оно будет
 * генерироваться автоматически путем авто инекремента
 */
@Entity(tableName = "products_item")
data class ProductDbModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo
    val name: String = "",

    @ColumnInfo
    val count: Int = 1,
    val isEnabled: Boolean = true
)