package com.mcmouse88.shoppinglist.data

import com.mcmouse88.shoppinglist.data.room.ProductDbModel
import com.mcmouse88.shoppinglist.domain.Product

/**
 * Чтобы можно было пользоваться классом [Product] в базе данных, нужно сделать
 * класс [mapper], который преобразовывает [Product] в [ProductDbModel], в нем будет два
 * метода, один преобразует сущность domain слоя в сущность data слоя, второй делает обратное
 * преобразование///
 */
class ProductListMapper {

    fun mapEntityToDbModule(product: Product): ProductDbModel =
        ProductDbModel(
            id = product.id,
            name = product.name,
            count = product.count,
            isEnabled = product.isEnabled
        )

    fun mapDbModuleToEntity(productDbModel: ProductDbModel): Product =
        Product(
            name = productDbModel.name,
            count = productDbModel.count,
            isEnabled = productDbModel.isEnabled,
            id = productDbModel.id
        )

    fun mapListDbModelToListEntity(listDbModel: List<ProductDbModel>): List<Product> =
        listDbModel.asSequence().map { mapDbModuleToEntity(it) }.toList()
}