package com.mcmouse88.shoppinglist.data

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.mcmouse88.shoppinglist.data.room.AppDatabase
import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

/**
 * После введения базы данных мы больше не можем использовать репозиторий как object
 */

class ShopListRepositoryImpl(application: Application) : ShopListRepository {

    private val productListDao = AppDatabase.getInstance(application).productListDao()
    private val mapper = ProductListMapper()

    override suspend fun createProductItem(product: Product) {
        productListDao.addShopItems(mapper.mapEntityToDbModule(product))
    }

    override suspend fun updateProductItem(product: Product) {
        productListDao.addShopItems(mapper.mapEntityToDbModule(product))
    }

    override suspend fun deleteProductItem(product: Product) {
         productListDao.deleteProductItem(mapper.mapEntityToDbModule(product))
    }

    override suspend fun getProductById(productId: Int): Product {
        val dbModel = productListDao.getProduct(productId)
        return mapper.mapDbModuleToEntity(dbModel)
    }

    /**
     * Класс [MediatorLiveData] это такой объект, который перехватывает события из другой
     * [LiveData], и может их как-то обрабатывать, для этого у него нужно установить источник
     * данных - объекты какой [LiveData] он будет перехватывать, в данном случае он будет
     * перехватывать объекты, которые лежат в базе данных, для этого вызывается метод
     * [addSource], который в качестве параметра принимает [LiveData], которую будет
     * перехватывать, вторым параметром он принимает лямбду, которая в качестве параметра
     * [List<ProductDbModel>], эта функция будет вызвана при каждом изменении в оригинальной
     * [LiveData], и вместо того, чтобы отправить данные на активити, они будут перехвачены,
     * и в лямбде можно будет указать, какие данные нам нужны, условия и т.п.
     */
    /*override fun getShopList(): LiveData<List<Product>> = MediatorLiveData<List<Product>>()
        .apply {
            addSource(productListDao.getProductList()) {
                value = mapper.mapListDbModelToListEntity(it)
            }
        }*/

    /**
     * Если же нам нужно только преобразование, то можно использовать класс [Transformations],
     * который под капотом также использует [MediatorLiveData]
     */
    override fun getShopList(): LiveData<List<Product>> =
        Transformations.map(productListDao.getProductList()) {
        mapper.mapListDbModelToListEntity(it)
    }
}