package com.mcmouse88.shoppinglist.data.room

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Чтобы методы не прерывали главный поток в случае долгой загрузки базы данных
 * нужно объявить их как suspend, к методу [getProductList] suspend добавлять не
 * нужно, так как объект возвращает объект [LiveData], а это значит, что он будет
 * выполнен на другом потоке
 */
@Dao
interface ProductListDao {

    @Query("select * from products_item")
    fun getProductList(): LiveData<List<ProductDbModel>>

    /**
     * Метод, который добавляет данные в таблицу, в случае возникновения конфлика,
     * возникающего при изменении элемента, новый элемент будет записан поверх старого
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addShopItems(product: ProductDbModel)

    @Delete
    suspend fun deleteProductItem(product: ProductDbModel)

    @Query("select * from products_item where id=:productId limit 1")
    suspend fun getProduct(productId: Int): ProductDbModel
}