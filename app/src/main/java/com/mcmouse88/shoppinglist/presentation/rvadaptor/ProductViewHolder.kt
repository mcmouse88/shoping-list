package com.mcmouse88.shoppinglist.presentation.rvadaptor

import android.view.View
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mcmouse88.shoppinglist.R
import com.mcmouse88.shoppinglist.databinding.ItemProductDisabledBinding

/**
 * Переделаем [ViewHolder] под [binding], для этого вместо [View] передадим в параметр конструктора
 * объект типа [binding] (который пока будет типа [ItemProductDisabledBinding], так как у нас два
 * типа item, мы будем пока использовать пока что один), так как [RecyclerView.ViewHolder] в
 * своем конструкторе содержит объект типа [View], мы передадим туда [binding.root], который
 * содержит в себе ссылку на [View], ссылки на элементы [View] больше не нужны, поэтому
 * их можно удалить. Чтобы использовать разные типы [binding], можно в конструктор передать
 * родительский тип [ViewDataBinding]
 */
class ProductViewHolder(
    val binding: ViewDataBinding
) : RecyclerView.ViewHolder(binding.root)