package com.mcmouse88.shoppinglist.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcmouse88.shoppinglist.data.ShopListRepositoryImpl
import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.usecase.DeleteProductItemUseCase
import com.mcmouse88.shoppinglist.domain.usecase.GetShopListUseCase
import com.mcmouse88.shoppinglist.domain.usecase.UpdateProductItemUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : ViewModel() {

    private val repository = ShopListRepositoryImpl(application)

    private val getShopListUseCase = GetShopListUseCase(repository)
    private val deleteProductItemUseCase = DeleteProductItemUseCase(repository)
    private val updateProductItemUseCase = UpdateProductItemUseCase(repository)

    /**
     * Так как во [ViewModel] нельзя использовать lifeCycleScope, так как он работает
     * только с жизненным циклом активити, пока создадим свою реализацию скоупа
     * через класс [CoroutineScope], в качестве праметра передадим ему корутин поток, а именно
     * предназначенный для чтения и записи фалов (в случае с базой данных он нам как раз подходит),
     * так же есть еще [Dispatchers.Main], который выполняется на главном потоке, и
     * [Dispatchers.Default] который используется при сложных вычислениях, и создает столько
     * потоков, сколько ядер у устройства. Также важный момент, что корутины работают только
     * с объектами, у которых есть жизненный цикл, при создании своей корутины, ее нужно отменить
     * в методе [onCleared]
     */
    // private val scope = CoroutineScope(Dispatchers.IO)

    val productList = getShopListUseCase.getShopList()


    /**
     * Получим список элементов, который будем передавать на экран через [LiveData],
     * для сохранения данных в ее объект используем метод [set] свойства [value]
     * (подходит для работы только в главном потоке (можно его вызывать только в
     * главном потоке), более универсальным является метод [postValue], который можно
     * вызывать из любого потока.
     */


    fun deleteProductItem(product: Product) {
        viewModelScope.launch {
            deleteProductItemUseCase.deleteProductItem(product)
        }

    }

    fun updateProductItem(product: Product) {
        viewModelScope.launch {
            val newProduct = product.copy(isEnabled = !product.isEnabled)
            updateProductItemUseCase.updateProductItem(newProduct)
        }
    }
}