package com.mcmouse88.shoppinglist.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.mcmouse88.shoppinglist.R
import com.mcmouse88.shoppinglist.databinding.ActivityMainBinding
import com.mcmouse88.shoppinglist.presentation.ProductItemActivity.Companion.newIntentAddItem
import com.mcmouse88.shoppinglist.presentation.ProductItemActivity.Companion.newIntentEditItem
import com.mcmouse88.shoppinglist.presentation.fragments.ProductItemFragment
import com.mcmouse88.shoppinglist.presentation.rvadaptor.ShopListAdapter
import com.mcmouse88.shoppinglist.presentation.viewmodel.MainViewModel
import com.mcmouse88.shoppinglist.presentation.viewmodel.MainViewModelFactory

class MainActivity : AppCompatActivity(), OnEditingFinishListener {

    private lateinit var binding: ActivityMainBinding

    private val factory: MainViewModelFactory by lazy {
        MainViewModelFactory(application)
    }

    private val model by viewModels<MainViewModel> { factory }

    private lateinit var shopListAdapter: ShopListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        model.productList.observe(this) {
            shopListAdapter.submitList(it)
        }

        binding.addNewItem.setOnClickListener {
            if (isOrientationPortrait()) {
                val intent = newIntentAddItem(this)
                startActivity(intent)
            } else {
                launchFragment(ProductItemFragment.newInstanceAddItem())
            }
        }
    }

    override fun onEditingFinished() {
        Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show()
        supportFragmentManager.popBackStack()
    }

    private fun setupRecyclerView() {
        with(binding.recyclerViewProductList) {
            shopListAdapter = ShopListAdapter()
            adapter = shopListAdapter
            recycledViewPool.setMaxRecycledViews(
                ShopListAdapter.VIEW_TYPE_ENABLED,
                ShopListAdapter.MAX_POOL_SIZE
            )
            recycledViewPool.setMaxRecycledViews(
                ShopListAdapter.VIEW_TYPE_DISABLED,
                ShopListAdapter.MAX_POOL_SIZE
            )
        }
        setupLongClick()
        setupClick()
        setupSwipe(binding.recyclerViewProductList)
    }

    private fun setupSwipe(recyclerViewProductList: RecyclerView) {
        val callback = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val item = shopListAdapter.currentList[viewHolder.adapterPosition]
                model.deleteProductItem(item)
            }
        }
        val itemTouchHelper = ItemTouchHelper(callback)
        itemTouchHelper.attachToRecyclerView(recyclerViewProductList)
    }

    private fun setupClick() {
        shopListAdapter.onProductItemClick = {
            if (isOrientationPortrait()) {
                val intent = newIntentEditItem(this, it.id)
                startActivity(intent)
            } else {
                launchFragment(ProductItemFragment.newInstanceEditItem(it.id))
            }
        }
    }

    private fun setupLongClick() {
        shopListAdapter.onProductItemLongClick = {
            model.updateProductItem(it)
        }
    }

    private fun isOrientationPortrait(): Boolean {
        return binding.productItemContainer == null
    }

    /**
     * Метод [addToBackStack] добавляет фрагмент в стек при вызове
     * (иначе закроется главное Активити при сохранении изменений,
     * в качестве параметра (String?) в данном случае можно передать null,
     * Вместо null можно передать навигацию на определенный фрагмент назад
     * по стеку. Пример этого я покажу в ветке [popBackStack/example].
     * Также перед каждым созданием фрагмента будем вызывать метод
     * [popBackStack], чтобы фрагменты не накапливались в стеке, если
     * фрагмента в стеке нет, то ничего не произойдет
     */
    private fun launchFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.product_item_container, fragment)
            .addToBackStack(null)
            .commit()
    }
}




