package com.mcmouse88.shoppinglist.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ProductItemViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProductItemViewModel::class.java)) {
            return ProductItemViewModel(application) as T
        }
        else throw RuntimeException("Unknown ViewModel class $modelClass")
    }
}