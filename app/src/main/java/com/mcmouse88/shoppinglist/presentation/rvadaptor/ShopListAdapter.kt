package com.mcmouse88.shoppinglist.presentation.rvadaptor

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mcmouse88.shoppinglist.R
import com.mcmouse88.shoppinglist.databinding.ItemProductDisabledBinding
import com.mcmouse88.shoppinglist.databinding.ItemProductEnabledBinding
import com.mcmouse88.shoppinglist.domain.Product

class ShopListAdapter : ListAdapter<Product, ProductViewHolder>(ProductItemDiffCallback()) {

    var onProductItemLongClick: ((Product) -> Unit)? = null

    var onProductItemClick: ((Product) -> Unit)? = null

    // Метод который определяет как создавать View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layout = when (viewType) {
            VIEW_TYPE_ENABLED -> R.layout.item_product_enabled
            VIEW_TYPE_DISABLED -> R.layout.item_product_disabled
            else -> throw RuntimeException("Unknown viewType: $viewType")
        }

        /**
         * Чтобы использовать разные типы [binding], нам нужно вызвать метод [inflate] у
         * класса [DateBindingUtil], в который вторым праметром передадим нужный нам
         * layout, но чтобы получить доступ к элемента [View] нам нужно явно привести
         * [binding] к нужному типу
         */
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            layout,
            parent,
            false
        )
        return ProductViewHolder(binding)
    }

    // Метод который определяет как изменять View (менять текст в TextView и т.д.)
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = getItem(position)
        val binding = holder.binding
        binding.root.setOnLongClickListener {
            onProductItemLongClick?.invoke(product)
            true
        }

        binding.root.setOnClickListener {
            onProductItemClick?.let { it(product) }
        }



        when(binding) {
            is ItemProductEnabledBinding -> {
                binding.product = product
            }
            is ItemProductDisabledBinding -> {
                binding.product = product
            }
        }
    }

    // Нужно переопределить метод, чтоб элементы отображались правильно
    // в зависимости от условия
    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item.isEnabled) {
            VIEW_TYPE_ENABLED
        } else {
            VIEW_TYPE_DISABLED
        }

    }

    companion object {
        const val VIEW_TYPE_ENABLED = 0
        const val VIEW_TYPE_DISABLED = 1

        /**
         * Создали константу для [pool RecyclerView].
         * Pool это объекты, хранящиеся в памяти [RecyclerView], и
         * если нужного [ViewType] нет в памяти (в [Pool], то вызывается метод
         * [onCreateViewHolder], после чего из [Pool] удаляется один элемен и
         * заменяет его необходимым (данная операция сказывается на производительности)
         */
        const val MAX_POOL_SIZE = 5
    }
}