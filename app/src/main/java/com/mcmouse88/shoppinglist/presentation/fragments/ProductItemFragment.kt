package com.mcmouse88.shoppinglist.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.mcmouse88.shoppinglist.databinding.FragmentProductItemBinding
import com.mcmouse88.shoppinglist.domain.Product.Companion.UNDEFINED_ID
import com.mcmouse88.shoppinglist.presentation.OnEditingFinishListener
import com.mcmouse88.shoppinglist.presentation.viewmodel.ProductItemViewModel
import com.mcmouse88.shoppinglist.presentation.viewmodel.ProductItemViewModelFactory

class ProductItemFragment : Fragment() {

    private val factory: ProductItemViewModelFactory by lazy {
        ProductItemViewModelFactory(requireActivity().application)
    }

    private val productItemModel: ProductItemViewModel by lazy {
        ViewModelProvider(this, factory)[ProductItemViewModel::class.java]
    }

    /**
     * Если фрагменту нужно о чем то сообщить Активити, то это
     * стоит делать через интерфейс, и переменную с поздней реализациейю
     * Данную переменную стоит инициализировать в методе
     * [onAttach], который вызывается в момент прикрепления
     * фрагмента к активити. Любая активити, которая будет
     * содержать в себе данный фрагмент, должна будет реализовывать
     * этот интерфейс
     */
    private lateinit var editingFinishListener: OnEditingFinishListener

    private var _binding: FragmentProductItemBinding? = null
    private val binding
        get() = _binding ?: throw RuntimeException("Fragment is null")

    private var screenMode: String = MODE_UNKNOWN
    private var productId: Int = UNDEFINED_ID


    /**
     * В данном методе создается макет фрагмента,
     * и все [View]
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d("livecycle", "onCreateView")
        /**
         * Здесь создадим сам макет вызвав у параметра [inflater]
         * класса [LayoutInflater] метод [inflate] в качестве аргумента
         * в который передадим макет созданного фрагмента из папки [res],
         * а также параметр [container] класса [ViewGroup] и false, позже подключим туда
         * [binding]
         */
        _binding = FragmentProductItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Обычно проверку на правильность аргументов далают в методе
     * [onCreate], однако если вызвать ее в других на этапе
     * создания [View] - [onCreateView] или [onViewCreated]
     * то приложение также будет работать
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("livecycle", "onCreate")
        super.onCreate(savedInstanceState)
        parseParams()
    }


    /**
     * Метод [onViewCreated], который вызывается уже после того, как [View создано]
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("livecycle", "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = productItemModel
        binding.lifecycleOwner = viewLifecycleOwner
        hideErrorInTextField(binding.etName) { productItemModel.resetErrorInputName() }
        hideErrorInTextField(binding.etCount) { productItemModel.resetErrorInputCount() }
        addRightScreenMode()
        productItemModel.shouldCloseScreen.observe(viewLifecycleOwner) {
            /**
             * чтобы получить текущее [Activity] во фрагменте можно вызвать
             * свойство [activity] или getActivity(), вернет нулабельный объект,
             * что является более безопасным, либо можно вызвать метод
             * [requireActivity] и получить не нулабельный объект, но тогда можно
             * получить [IllegalStateException], тоже самое при работе с [context] и
             * [requireContext] и т.п.
             */
            editingFinishListener.onEditingFinished()
        }
    }

    /**
     * Данный метод прикрепляет фрагмент к Активити
     */
    override fun onAttach(context: Context) {
        Log.d("livecycle", "onAttach")
        super.onAttach(context)
        if (context is OnEditingFinishListener) {
            editingFinishListener = context
        } else {
            throw RuntimeException("Activity must implement OnEditingFinishListener")
        }
    }


    /**
     * При отображении фрагмента он сначала прикрепляется к [Activity],
     * потом из макета создается [View] в методе [onCreateView]
     */
    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        }*/


    /**
     * Метод который проверяет пришли ли правильные аргументы во фрагмент,
     * вызываем метод [requireArguments], который возвращает текущие
     * аргументы, если они отсутствуют, то приложение сразу упадет с ошибкой
     * (соответственно мы сделали что-то не так), если все нормально, то
     * вернет не нулабельный тип, далее проверяем содержат ли аргументы
     * все необходимые ключи и значения, если же нет выбрасываем исключения
     */
    private fun parseParams() {
        val args = requireArguments()

        if (!args.containsKey(SCREEN_MODE)) {
            throw RuntimeException("Param screen mode is absent!")
        }

        val mode = args.getString(SCREEN_MODE)
        if (mode != MODE_EDIT && mode != MODE_ADD) {
            throw RuntimeException("Unknown type of mode: $mode")
        }

        screenMode = mode

        if (screenMode == MODE_EDIT) {
            if (!args.containsKey(PRODUCT_ID)) {
                throw RuntimeException("Param item is absent!")
            }
            productId = args.getInt(PRODUCT_ID, UNDEFINED_ID)
        }
    }


    private fun launchEditMode() {
        productItemModel.getProductById(productId)
        productItemModel.productItem.observe(viewLifecycleOwner) {
            binding.saveButton.setOnClickListener {
                productItemModel.updateProductItem(
                    binding.etName.text?.toString(),
                    binding.etCount.text?.toString()
                )
            }
        }
    }

    private fun launchAddMode() {
        binding.saveButton.setOnClickListener {
            productItemModel.createProductItem(binding.etName.text?.toString(), binding.etCount.text?.toString())
        }
    }

    private fun hideErrorInTextField(editView: TextInputEditText, callBack: () -> Unit) {
        editView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                callBack()
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })
    }

    private fun addRightScreenMode() {
        when (screenMode) {
            MODE_EDIT -> launchEditMode()
            MODE_ADD -> launchAddMode()
        }
    }

    /**
     * В данном методе нельзя передать в метод [observe] слово
     * [this], как это делается в [Activity], так как [View] уже
     * может не сущестовать, а [Fragment] еще может быть жив (приложение бы упало).
     * По этому в метод [observe] мы передаем [viewLifecycleOwner] то есть, свойство,
     * отвечающее за жизненный цикл [View], и при завершении цикла жизни [View] метод
     * [observe] автоматически отпишется от [LiveData]
     */

    companion object {
        private const val SCREEN_MODE = "extra_mode"
        private const val PRODUCT_ID = "extra_product_id"
        private const val MODE_EDIT = "mode_edit"
        private const val MODE_ADD = "mode_add"
        private const val MODE_UNKNOWN = ""

        /**
         * Во фрагменты правильно передавать аргументы не в конструктор
         * (приложение падает при перерисовке фрагмента - повороте экрана, смена языка и т.п.),
         * а в метод
         */
        fun newInstanceAddItem(): ProductItemFragment {
            return ProductItemFragment().apply {
                arguments = Bundle().apply { putString(SCREEN_MODE, MODE_ADD) }
            }
        }

        fun newInstanceEditItem(productId: Int): ProductItemFragment {
            return ProductItemFragment().apply {
                arguments = Bundle().apply {
                    putString(SCREEN_MODE, MODE_EDIT)
                    putInt(PRODUCT_ID, productId)
                }
            }
        }
    }

    override fun onDestroy() {
        Log.d("livecycle", "onDestroy")
        super.onDestroy()
        _binding = null
    }
}
