package com.mcmouse88.shoppinglist.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcmouse88.shoppinglist.data.ShopListRepositoryImpl
import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.usecase.CreateProductItemUseCase
import com.mcmouse88.shoppinglist.domain.usecase.GetProductByIdUseCase
import com.mcmouse88.shoppinglist.domain.usecase.UpdateProductItemUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.lang.NumberFormatException

class ProductItemViewModel(application: Application) : ViewModel() {

    private val repository = ShopListRepositoryImpl(application)
    private val createProductItemUseCase = CreateProductItemUseCase(repository)
    private val updateProductItemUseCase = UpdateProductItemUseCase(repository)
    private val getProductByIdUseCase = GetProductByIdUseCase(repository)


    private val _errorInputName = MutableLiveData<Boolean>()
    val errorInputName: LiveData<Boolean>
        get() = _errorInputName

    private val _errorInputCount = MutableLiveData<Boolean>()
    val errorInputCount: LiveData<Boolean>
        get() = _errorInputCount

    private val _productItem = MutableLiveData<Product>()
    val productItem: LiveData<Product>
        get() = _productItem

    private val _shouldCloseScreen = MutableLiveData<Unit>()
    val shouldCloseScreen: LiveData<Unit>
        get() = _shouldCloseScreen

    fun createProductItem(inputName: String?, inputCount: String?) {
        val name = formatInputName(inputName)
        val count = formatInputCount(inputCount)
        val fieldsValidate = validateInput(name, count)
        if (fieldsValidate) {
            viewModelScope.launch {
                val newProduct = Product(name, count)
                createProductItemUseCase.createProductItem(newProduct)
                finishWork()
            }
        }
    }

    fun updateProductItem(inputName: String?, inputCount: String?) {
        val name = formatInputName(inputName)
        val count = formatInputCount(inputCount)
        val fieldValidate = validateInput(name, count)
        if (fieldValidate) {
            _productItem.value?.let {
                viewModelScope.launch {
                    val updateProduct = it.copy(name = name, count = count)
                    updateProductItemUseCase.updateProductItem(updateProduct)
                    finishWork()
                }
            }
        }
    }

    fun getProductById(productId: Int) {
        viewModelScope.launch {
            val item = getProductByIdUseCase.getProductById(productId)
            _productItem.value = item
        }
    }

    private fun formatInputName(inputName: String?): String {
        return inputName?.trim() ?: ""
    }

    private fun formatInputCount(inputCount: String?): Int {
        return try {
            inputCount?.trim()?.toInt() ?: 0
        } catch (e: NumberFormatException) {
            0
        }
    }

    private fun validateInput(name: String, count: Int): Boolean {
        var result = true
        if (name.isBlank()) {
            _errorInputName.value = true
            result = false
        }
        if (count <= 0) {
            _errorInputCount.value = true
            result = false
        }
        return result
    }

    fun resetErrorInputName() {
        _errorInputName.value = false
    }

    fun resetErrorInputCount() {
        _errorInputCount.value = false
    }

    private fun finishWork() {
        _shouldCloseScreen.value = Unit
    }
}