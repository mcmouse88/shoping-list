package com.mcmouse88.shoppinglist.presentation.rvadaptor

import androidx.recyclerview.widget.DiffUtil
import com.mcmouse88.shoppinglist.domain.Product

class ProductItemDiffCallback : DiffUtil.ItemCallback<Product>() {

    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem == newItem
    }
}