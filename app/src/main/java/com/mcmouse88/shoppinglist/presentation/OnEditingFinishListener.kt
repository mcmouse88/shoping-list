package com.mcmouse88.shoppinglist.presentation

interface OnEditingFinishListener {
    fun onEditingFinished()
}