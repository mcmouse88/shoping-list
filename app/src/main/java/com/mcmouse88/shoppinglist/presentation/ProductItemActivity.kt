package com.mcmouse88.shoppinglist.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mcmouse88.shoppinglist.R
import com.mcmouse88.shoppinglist.databinding.ActivityProductItemBinding
import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.presentation.fragments.ProductItemFragment

class ProductItemActivity : AppCompatActivity(), OnEditingFinishListener {

    private var screenMode = MODE_UNKNOWN
    private var productId = Product.UNDEFINED_ID


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_item)
        parseIntent()
        if (savedInstanceState == null) {
            addRightScreenMode()
        }
    }

    private fun parseIntent() {
        if (!intent.hasExtra(EXTRA_SCREEN_MODE)) {
            throw RuntimeException("Param screen mode is absent")
        }
        val mode = intent.getStringExtra(EXTRA_SCREEN_MODE)
        if (mode != MODE_EDIT && mode != MODE_ADD) {
            throw RuntimeException("Unknown screen mode $mode")
        }
        screenMode = mode

        if (screenMode == MODE_EDIT) {
            if (!intent.hasExtra(EXTRA_PRODUCT_ID)) {
                throw RuntimeException("Param productId is absent")
            }
            productId = intent.getIntExtra(EXTRA_PRODUCT_ID, Product.UNDEFINED_ID)
        }
    }

    override fun onEditingFinished() {
        Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun addRightScreenMode() {
        val fragment = when (screenMode) {
            MODE_EDIT -> ProductItemFragment.newInstanceEditItem(productId)
            MODE_ADD -> ProductItemFragment.newInstanceAddItem()
            else -> throw RuntimeException("Unknown screen mode - $screenMode")
        }
        /**
         * Чтобы получить фрагмент, нужно вызвать свойство
         * [supportFragmentManager], если хотим отобразить [Fragment] в каком-то
         * контейнере, нужно вызвать метод [beginTransaction], после вызываем метод
         * [add], в который в качестве первого параметра передаем id контейнера,
         * а в качестве второго передаем сам фрагмент, который мы собрали выше
         * в зависимости от выбранного режима, и в конце нужно вызвать метод
         * [commit], который запустит транзакцию на выполнение.
         * Далее вместо метода [add] лучше использовать метод [replace], так как
         * он будет заменять фрагмент в контейнере, а не помещать несколько
         * фрагментов в один контейнер, как это делает метод [add].
         * Также в методе [onCreate] мы проверим [savedInstanceState] на
         * null, и если он не равен null, то [Activity] уже создано, и
         * заново в ручную создавать нам фрагмент уже не нужно, система
         * сделает это за нас
         */
        supportFragmentManager.beginTransaction()
            .replace(R.id.product_item_container, fragment)
            .commit()
    }

    companion object {
        private const val EXTRA_SCREEN_MODE = "extra_mode"
        private const val EXTRA_PRODUCT_ID = "extra_product_id"
        private const val MODE_EDIT = "mode_edit"
        private const val MODE_ADD = "mode_add"
        private const val MODE_UNKNOWN = ""

        fun newIntentAddItem(context: Context): Intent {
            val intent = Intent(context, ProductItemActivity::class.java)
            intent.putExtra(EXTRA_SCREEN_MODE, MODE_ADD)
            return intent
        }

        fun newIntentEditItem(context: Context, productId: Int): Intent {
            val intent = Intent(context, ProductItemActivity::class.java)
            intent.putExtra(EXTRA_SCREEN_MODE, MODE_EDIT)
            intent.putExtra(EXTRA_PRODUCT_ID, productId)
            return intent
        }
    }
}