package com.mcmouse88.shoppinglist.domain

data class Product(
    val name: String,
    val count: Int,
    val isEnabled: Boolean = true,
    var id: Int = UNDEFINED_ID
) {
    companion object {
        const val UNDEFINED_ID = 0
    }
}
