package com.mcmouse88.shoppinglist.domain.usecase

import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

class DeleteProductItemUseCase(private val repository: ShopListRepository) {

    suspend fun deleteProductItem(product: Product) {
        repository.deleteProductItem(product)
    }
}