package com.mcmouse88.shoppinglist.domain.usecase

import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

class GetProductByIdUseCase(private val repository: ShopListRepository) {

    suspend fun getProductById(productId: Int): Product {
        return repository.getProductById(productId)
    }
 }