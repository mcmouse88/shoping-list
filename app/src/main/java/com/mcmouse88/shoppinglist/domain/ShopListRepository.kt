package com.mcmouse88.shoppinglist.domain

import androidx.lifecycle.LiveData

/**
 * По принципам [SOLID] приложение стоит делить на слои, как минимум на три.
 * [Data] - в котором будет реализовано работа с данными (базы данных, получение
 * данных из интернета и т.д. [Presentation] - в котором будет реализовано
 * отоброжение информации в самом приложении (активити, viewmodel, экраны, фрагменты
 * и т.п. [Domain] - в котором будет реализована бизнес логика приложения
 * (основные операции в приложении, в данном приложении - создание, удаление, изменение
 * элементов [Product].
 * Создадим интерфейс [ShopListRepository], который объединяет действия
 * [use case] (один класс одно действие, которое может выполнять пользоваетель
 * в приложении) [CreateProductItemUseCase], [UpdateProductItemUseCase],
 * [DeleteProductItemUseCase], [GetProductByIdUseCase] и [GetShopListUseCase].
 * Далее в слое [data] создадим объект [ShopListRepositoryImpl], который
 * будет реализовывать интерфейс [ShopListRepository].
 */
interface ShopListRepository {

    suspend fun createProductItem(product: Product)

    suspend fun updateProductItem(product: Product)

    suspend fun deleteProductItem(product: Product)

    suspend fun getProductById(productId: Int): Product

    fun getShopList(): LiveData<List<Product>>
}