package com.mcmouse88.shoppinglist.domain.usecase

import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

class CreateProductItemUseCase(private val repository: ShopListRepository) {

    suspend fun createProductItem(product: Product) {
        repository.createProductItem(product)
    }
}