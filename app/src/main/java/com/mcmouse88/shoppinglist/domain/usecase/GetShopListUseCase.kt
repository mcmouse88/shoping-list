package com.mcmouse88.shoppinglist.domain.usecase

import androidx.lifecycle.LiveData
import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

class GetShopListUseCase(private val repository: ShopListRepository) {

    fun getShopList(): LiveData<List<Product>> {
        return repository.getShopList()
    }

}