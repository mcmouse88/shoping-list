package com.mcmouse88.shoppinglist.domain.usecase

import com.mcmouse88.shoppinglist.domain.Product
import com.mcmouse88.shoppinglist.domain.ShopListRepository

class UpdateProductItemUseCase(private val repository: ShopListRepository) {

    suspend fun updateProductItem(product: Product) {
        repository.updateProductItem(product)
    }
}