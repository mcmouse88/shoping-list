## App Shopping List

---

[![pipeline status](https://gitlab.com/mcmouse88/shoping-list/badges/main/pipeline.svg)](https://gitlab.com/mcmouse88/shoping-list/-/commits/main)

[Lint Report](https://mcmouse88.gitlab.io/-/shoping-list/-/jobs/3178678175/artifacts/app/lint/reports/lint-results-debug.html)

[Test Coverage](https://mcmouse88.gitlab.io/-/shoping-list/-/jobs/3178749113/artifacts/app/build/reports/tests/testDebugUnitTest/index.html)